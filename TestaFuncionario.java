public class TestaFuncionario{
	public static void main(String[] args){
		Funcionario umFuncionario = new Funcionario();
		Data umDataEntrada = new Data();
		
		umFuncionario.nome = "Ygor";
		umFuncionario.salario = 1000;
		umFuncionario.recebeAumento(150);
		umFuncionario.numeroFaltas(3);
		umFuncionario.dataEntradaBanco = umDataEntrada;
		umDataEntrada.dia=07;
		umDataEntrada.mes=10;
		umDataEntrada.ano=2014;
		umFuncionario.mostraPrints();		
	}
}
